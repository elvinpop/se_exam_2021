import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 {
    public static void main(String[] args){
        new GUI();
    }
}

class GUI extends JFrame {

    private JTextField jtfOne;
    private JTextField jtfTwo;
    private JButton jButton;

    public GUI() {
        setTitle("Subject 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {
        int width = 140;
        int height = 20;

        this.setLayout(null);

        this.jtfOne = new JTextField();
        this.jtfTwo = new JTextField();
        this.jButton = new JButton();

        jtfTwo.setEditable(false);
        jButton.addActionListener(new ButtonAction());

        jtfOne.setBounds(20, 50, width, height);
        jtfTwo.setBounds(20, 90, width, height);
        jButton.setBounds(20, 150, width, height);

        add(jtfOne); add(jtfTwo); add(jButton);
    }

    class ButtonAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            GUI.this.jtfTwo.setText(GUI.this.jtfOne.getText());
        }
    }
}