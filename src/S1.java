public class S1 {
}

class A {

}

class B extends A {
    private String param;
    private C c;

    public B() {
        this.c = new C();
    }

    public void funcZ(Z z) {

    }
}

class C {

}

class D {
    private B b;

    public void setB(B b) {
        this.b = b;
    }

    public void f() {

    }
}

class E {
    private B b;
}

class Z {
    public void g() {

    }
}